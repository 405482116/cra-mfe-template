import styled from "styled-components";

const HeaderWrapper = styled.header`
  display: flex;
  height: 60px;
  justify-content: space-between;
  align-items: center;
`;

export default HeaderWrapper;

import React from "react";
import Cookies from "js-cookie";
import HeaderWrapper from "./HeaderWrapper";
import { useFormatMessage, SupportedLocales } from "./app/intlHelpers";
import { Language, SwitchLanguage } from "./features/language";

const Component: React.FC = () => {
  const formatMessage = useFormatMessage();
  return (
    <>
      <HeaderWrapper>
        Translated content:
        {formatMessage("app.title") /* only valid keys can be passed here */}
      </HeaderWrapper>
    </>
  );
};

const App: React.FC = (props) => {
  const __locale = (Cookies.get("locale") as SupportedLocales) || "en-us";
  const [locale, setLocale] = React.useState<SupportedLocales>(__locale);
  // const [messages, setMessages] = React.useState<LocaleMessages | null>(null);

  // React.useEffect(() => {
  //   importMessages(locale).then(setMessages);
  // }, [locale]);

  return (
    <>
      <SwitchLanguage setLocale={setLocale} />
      <Language locale={locale}>
        <Component {...props} />
      </Language>
      {/* {messages ? (
        <IntlProvider locale={locale} messages={messages}>
          <Component {...props} />
        </IntlProvider>
      ) : null} */}
    </>
  );
};

export default App;

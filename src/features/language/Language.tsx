import React, { ElementType, ReactElement } from "react";
import Cookies from "js-cookie";
import { useAppSelector, useAppDispatch } from "../../app/hooks";
import { selectMessages, languageAsync } from "./languageSlice";

import {
  useFormatMessage,
  IntlProvider,
  LocaleMessages,
  importMessages,
  SupportedLocales,
} from "../../app/intlHelpers";

interface Props {
  children: ReactElement;
  locale: SupportedLocales;
}

const Language: React.FC<Props> = (props) => {
  const messages: LocaleMessages | null = useAppSelector(selectMessages);
  console.log("🚀 ~ file: Language.tsx ~ line 21 ~ messages", messages)
  const dispatch = useAppDispatch();

  //   const __locale = (Cookies.get("locale") as SupportedLocales) || "en-us";
  //   const [locale, setLocale] = React.useState<SupportedLocales>(__locale);

  React.useEffect(() => {
    dispatch(
      languageAsync({
        // load remote nls file
        path: "locale/sampile.json",
        locale: props.locale,
      })
    );
  }, [props.locale]);

  return (
    <>
      {messages ? (
        <IntlProvider locale={props.locale} messages={messages}>
          {props.children}
        </IntlProvider>
      ) : null}
    </>
  );
};

export default Language;

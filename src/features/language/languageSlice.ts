import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { SupportedLocales } from "../../app/intlHelpers";
import { RootState } from "../../app/store";
import { fetchLocaleMessages } from "./localeMessagesAPI";

export interface LanguageState {
  messages: any;
  status: "idle" | "loading" | "failed";
}

const initialState: LanguageState = {
  messages: null,
  status: "idle",
};

// The function below is called a thunk and allows us to perform async logic. It
// can be dispatched like a regular action: `dispatch(incrementAsync(10))`. This
// will call the thunk with the `dispatch` function as the first argument. Async
// code can then be executed and other actions can be dispatched. Thunks are
// typically used to make async requests.

export interface LanguageAsyncArg {
  path: string;
  locale: SupportedLocales;
}
export const languageAsync = createAsyncThunk(
  "counter/fetchLocaleMessages",
  async (arg: LanguageAsyncArg) => {
    const response = await fetchLocaleMessages(arg.path);
    // The value we return becomes the `fulfilled` action payload
    return response.data[arg.locale];
  }
);

export const counterSlice = createSlice({
  name: "language",
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {},
  // The `extraReducers` field lets the slice handle actions defined elsewhere,
  // including actions generated by createAsyncThunk or in other slices.
  extraReducers: (builder) => {
    builder
      .addCase(languageAsync.pending, (state) => {
        state.status = "loading";
      })
      .addCase(languageAsync.fulfilled, (state, action) => {
        state.status = "idle";
        state.messages = action.payload;
      })
      .addCase(languageAsync.rejected, (state, action) => {
        state.status = "failed";
        state.messages = action.payload;
      });
  },
});

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state: RootState) => state.counter.value)`
export const selectMessages = (state: RootState) => state.lanugage.messages;

export default counterSlice.reducer;

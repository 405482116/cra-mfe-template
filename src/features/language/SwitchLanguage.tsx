import React from "react";
import Cookies from "js-cookie";

interface Props {
  setLocale: Function;
}
const SwitchLanguage: React.FC<Props> = ({ setLocale }) => {
  const list = ["en", "cn", "hk"];
  const changeLanguage = (item: string) => {
    switch (item) {
      case "cn":
        Cookies.set("locale", "zh-cn");
        setLocale("zh-cn");
        break;
      case "hk":
        Cookies.set("locale", "zh-hk");
        setLocale("zh-hk");
        break;
      default:
        Cookies.set("locale", "en-us");
        setLocale("en-us");
        break;
    }
  };
  return (
    <>
      {list.map((item) => (
        <button key={item} type="button" onClick={() => changeLanguage(item)}>
          {" "}
          {item}
        </button>
      ))}
    </>
  );
};

export default SwitchLanguage;

import * as React from "react";
import { useIntl, IntlProvider as IntlProvider_ } from "react-intl";
import type { PrimitiveType } from "intl-messageformat";

import * as sourceOfTruth from "../local/en.json";

export type LocaleMessages = typeof sourceOfTruth;
export type LocaleKey = keyof LocaleMessages;

export function useFormatMessage(): (
  id: LocaleKey, // only accepts valid keys, not any string
  values?: Record<string, PrimitiveType>
) => string {
  const intl = useIntl();
  return (id, values) => intl.formatMessage({ id }, values);
}

export type SupportedLocales = "en-us" | "zh-cn" | "zh-hk";

export function importMessages(
  locale: SupportedLocales
): Promise<LocaleMessages> {
  switch (locale) {
    case "zh-hk":
      return import("../local/hk.json");
    case "zh-cn":
      return import("../local/cn.json");
    default:
      return import("../local/en.json");
  }
}
export const IntlProvider: React.FC<
  Omit<React.ComponentProps<typeof IntlProvider_>, "messages"> & {
    messages: LocaleMessages;
  }
> = (props) => <IntlProvider_ {...props} />;
